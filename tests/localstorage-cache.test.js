import { LocalStorageCache } from "../src/localstorage-cache";
jest.mock("../localstorage-cache");
const fakeLocalStorage = (function () {
  let store = {};

  return {
    getItem: function (key) {
      return store[key] || null;
    },
    setItem: function (key, value) {
      store[key] = value.toString();
    },
    removeItem: function (key) {
      delete store[key];
    },
    clear: function () {
      store = {};
    },
  };
})();

describe("set", () => {
  beforeAll(() => {
    Object.defineProperty(window, "localStorage", {
      value: fakeLocalStorage,
    });
  });

  it("saves the key to the storage", () => {
    const cache = LocalStorageCache();
    LocalStorageCache.set("swag", { poop: "swag" });
    expect(window.localStorage.getItem("the-key")).toEqual("fake-value");
  });
});
