export default class LocalStorageCache {
  prefix = "lsc"; //Prefix for keys
  CACHE_DATA_KEY = `${this.prefix}-cacheData`;
  cacheExpiryTime = 3600; //Amount of time to cache data for in seconds
  bucket = "";
  buckets = [];

  constructor(cacheExpiryTime, prefix) {
    this.cacheExpiryTime = cacheExpiryTime ?? 3600;
    this.prefix = prefix ?? "lsc";
    this.fetchBucketsFromLocalStorage();
  }

  set(key, value, cacheExpiryTime) {
    let expiryDateInMilliseconds = this.calculateExpiryDate(cacheExpiryTime);
    let valueToSet = { value: value, expiry: expiryDateInMilliseconds };

    try {
      valueToSet = JSON.stringify(valueToSet);
    } catch (e) {
      console.log(`Couldn't convert value to JSON, e: ${e}`);
      return false;
    }

    try {
      setItem(this.generateKey(key), valueToSet);
    } catch (e) {
      if (isLocalStorageOutOfSpace(e)) {
        let targetSize = valueToSet.length;
        let storedItems = [];
        this.eachKey((key) => {
          let item = getItem(key);
          let { expiry } = JSON.parse(item);
          storedItems.push({ key, value: item, expiry });
        });
        storedItems.sort((a, b) => a.expiry - b.expiry);

        while (storedItems.length && targetSize > 0) {
          let item = storedItems.shift();
          console.log(item);
          removeItem(key);
          targetSize -= item.value.length;
        }
        try {
          setItem(this.generateKey(key), valueToSet);
        } catch (e) {
          console.log(`Couldn't add key: ${key} to cache, e: ${e}`);
          return false;
        }
      } else {
        console.log(`Couldn't add key: ${key} to cache, e: ${e}`);
        return false;
      }
    }
    return true;
  }
  get(key) {
    key = this.generateKey(key);

    let item = getItem(key);

    if (!item || removeExpiredItem(key)) {
      return null;
    }
    let { value } = JSON.parse(item);
    return value;
  }

  each(regExp, callback) {
    for (const key in localStorage) {
      if (key.match(regExp)) {
        callback(key);
      }
    }
  }

  eachKeyInBucket(callback, bucket) {
    let regExp = new RegExp(`^${this.prefix}-${bucket ?? this.bucket}(.*)`);
    this.each(regExp, callback);
  }
  eachKey(callback) {
    let regExp = new RegExp(`^${this.prefix}-(.*)`);
    this.each(regExp, callback);
  }

  flush(expired = true) {
    let cb = expired ? removeExpiredItem : removeItem;
    this.eachKey((key) => cb(key));
  }
  flushBucket(expired = true, bucket) {
    bucket = bucket ?? this.bucket;
    let cb = expired ? removeExpiredItem : removeItem;
    this.eachKeyInBucket((key) => cb(key), bucket);
  }
  buckets() {
    return this.buckets;
  }

  generateKey(key) {
    return `${this.prefix}${this.bucket ? `-${this.bucket}` : ""}-${key}`;
  }

  fetchBucketsFromLocalStorage() {
    let buckets = localStorage.getItem(this.CACHE_DATA_KEY);
    if (buckets) {
      this.buckets = JSON.parse(buckets);
    } else {
      this.buckets = [];
    }
  }
  setBucket(bucketName) {
    let sanitizedString = removeNonAlphaNumeric(bucketName);
    if (this.buckets.includes(sanitizedString)) {
      this.bucket = sanitizedString;
      return;
    }
    let newBuckets = JSON.stringify([...this.buckets, sanitizedString]);
    try {
      setItem(this.CACHE_DATA_KEY, newBuckets);
    } catch (e) {
      return false;
    }
    this.bucket = sanitizedString;
  }
  calculateExpiryDate(cacheExpiryTime) {
    let expiry = cacheExpiryTime ?? this.cacheExpiryTime;
    let expiryDateInMilliseconds = new Date(
      Date.now() + expiry * 1000
    ).getTime();
    return expiryDateInMilliseconds;
  }
}

function setItem(key, value) {
  return localStorage.setItem(key, value);
}

function removeItem(key) {
  return localStorage.removeItem(key);
}

function getItem(key) {
  return localStorage.getItem(key);
}

function isExpired(expiryDateInMilliseconds) {
  let currentTime = getCurrentTimeInMS();
  return currentTime >= expiryDateInMilliseconds;
}

function removeExpiredItem(key) {
  let item = getItem(key);
  if (item) {
    let { expiry } = JSON.parse(item);
    if (isExpired(expiry)) {
      removeItem(key);
      return true;
    }
  }
}

function isLocalStorageOutOfSpace(e) {
  return (
    e &&
    (e.name === "QUOTA_EXCEEDED_ERR" ||
      e.name === "NS_ERROR_DOM_QUOTA_REACHED" ||
      e.name === "QuotaExceededError")
  );
}

function getCurrentTimeInMS() {
  return Date.now();
}

function removeNonAlphaNumeric(string) {
  return string.replace(/[^0-9a-z]/gi, "");
}
