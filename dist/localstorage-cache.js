"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var LocalStorageCache = /*#__PURE__*/function () {
  //Prefix for keys
  //Amount of time to cache data for in seconds
  function LocalStorageCache(cacheExpiryTime, prefix) {
    _classCallCheck(this, LocalStorageCache);

    _defineProperty(this, "prefix", "lsc");

    _defineProperty(this, "CACHE_DATA_KEY", "".concat(this.prefix, "-cacheData"));

    _defineProperty(this, "cacheExpiryTime", 3600);

    _defineProperty(this, "bucket", "");

    _defineProperty(this, "buckets", []);

    this.cacheExpiryTime = cacheExpiryTime !== null && cacheExpiryTime !== void 0 ? cacheExpiryTime : 3600;
    this.prefix = prefix !== null && prefix !== void 0 ? prefix : "lsc";
    this.fetchBucketsFromLocalStorage();
  }

  _createClass(LocalStorageCache, [{
    key: "set",
    value: function set(key, value, cacheExpiryTime) {
      var expiryDateInMilliseconds = this.calculateExpiryDate(cacheExpiryTime);
      var valueToSet = {
        value: value,
        expiry: expiryDateInMilliseconds
      };

      try {
        valueToSet = JSON.stringify(valueToSet);
      } catch (e) {
        console.log("Couldn't convert value to JSON, e: ".concat(e));
        return false;
      }

      try {
        setItem(this.generateKey(key), valueToSet);
      } catch (e) {
        if (isLocalStorageOutOfSpace(e)) {
          var targetSize = valueToSet.length;
          var storedItems = [];
          this.eachKey(function (key) {
            var item = getItem(key);

            var _JSON$parse = JSON.parse(item),
                expiry = _JSON$parse.expiry;

            storedItems.push({
              key: key,
              value: item,
              expiry: expiry
            });
          });
          storedItems.sort(function (a, b) {
            return a.expiry - b.expiry;
          });

          while (storedItems.length && targetSize > 0) {
            var item = storedItems.shift();
            console.log(item);
            removeItem(key);
            targetSize -= item.value.length;
          }

          try {
            setItem(this.generateKey(key), valueToSet);
          } catch (e) {
            console.log("Couldn't add key: ".concat(key, " to cache, e: ").concat(e));
            return false;
          }
        } else {
          console.log("Couldn't add key: ".concat(key, " to cache, e: ").concat(e));
          return false;
        }
      }

      return true;
    }
  }, {
    key: "get",
    value: function get(key) {
      key = this.generateKey(key);
      var item = getItem(key);

      if (!item || removeExpiredItem(key)) {
        return null;
      }

      var _JSON$parse2 = JSON.parse(item),
          value = _JSON$parse2.value;

      return value;
    }
  }, {
    key: "each",
    value: function each(regExp, callback) {
      for (var key in localStorage) {
        if (key.match(regExp)) {
          callback(key);
        }
      }
    }
  }, {
    key: "eachKeyInBucket",
    value: function eachKeyInBucket(callback, bucket) {
      var regExp = new RegExp("^".concat(this.prefix, "-").concat(bucket !== null && bucket !== void 0 ? bucket : this.bucket, "(.*)"));
      this.each(regExp, callback);
    }
  }, {
    key: "eachKey",
    value: function eachKey(callback) {
      var regExp = new RegExp("^".concat(this.prefix, "-(.*)"));
      this.each(regExp, callback);
    }
  }, {
    key: "flush",
    value: function flush() {
      var expired = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
      var cb = expired ? removeExpiredItem : removeItem;
      this.eachKey(function (key) {
        return cb(key);
      });
    }
  }, {
    key: "flushBucket",
    value: function flushBucket() {
      var _bucket;

      var expired = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
      var bucket = arguments.length > 1 ? arguments[1] : undefined;
      bucket = (_bucket = bucket) !== null && _bucket !== void 0 ? _bucket : this.bucket;
      var cb = expired ? removeExpiredItem : removeItem;
      this.eachKeyInBucket(function (key) {
        return cb(key);
      }, bucket);
    }
  }, {
    key: "buckets",
    value: function buckets() {
      return this.buckets;
    }
  }, {
    key: "generateKey",
    value: function generateKey(key) {
      return "".concat(this.prefix).concat(this.bucket ? "-".concat(this.bucket) : "", "-").concat(key);
    }
  }, {
    key: "fetchBucketsFromLocalStorage",
    value: function fetchBucketsFromLocalStorage() {
      var buckets = localStorage.getItem(this.CACHE_DATA_KEY);

      if (buckets) {
        this.buckets = JSON.parse(buckets);
      } else {
        this.buckets = [];
      }
    }
  }, {
    key: "setBucket",
    value: function setBucket(bucketName) {
      var sanitizedString = removeNonAlphaNumeric(bucketName);

      if (this.buckets.includes(sanitizedString)) {
        this.bucket = sanitizedString;
        return;
      }

      var newBuckets = JSON.stringify([].concat(_toConsumableArray(this.buckets), [sanitizedString]));

      try {
        setItem(this.CACHE_DATA_KEY, newBuckets);
      } catch (e) {
        return false;
      }

      this.bucket = sanitizedString;
    }
  }, {
    key: "calculateExpiryDate",
    value: function calculateExpiryDate(cacheExpiryTime) {
      var expiry = cacheExpiryTime !== null && cacheExpiryTime !== void 0 ? cacheExpiryTime : this.cacheExpiryTime;
      var expiryDateInMilliseconds = new Date(Date.now() + expiry * 1000).getTime();
      return expiryDateInMilliseconds;
    }
  }]);

  return LocalStorageCache;
}();

exports["default"] = LocalStorageCache;

function setItem(key, value) {
  return localStorage.setItem(key, value);
}

function removeItem(key) {
  return localStorage.removeItem(key);
}

function getItem(key) {
  return localStorage.getItem(key);
}

function isExpired(expiryDateInMilliseconds) {
  var currentTime = getCurrentTimeInMS();
  return currentTime >= expiryDateInMilliseconds;
}

function removeExpiredItem(key) {
  var item = getItem(key);

  if (item) {
    var _JSON$parse3 = JSON.parse(item),
        expiry = _JSON$parse3.expiry;

    if (isExpired(expiry)) {
      removeItem(key);
      return true;
    }
  }
}

function isLocalStorageOutOfSpace(e) {
  return e && (e.name === "QUOTA_EXCEEDED_ERR" || e.name === "NS_ERROR_DOM_QUOTA_REACHED" || e.name === "QuotaExceededError");
}

function getCurrentTimeInMS() {
  return Date.now();
}

function removeNonAlphaNumeric(string) {
  return string.replace(/[^0-9a-z]/gi, "");
}