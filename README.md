# localstorage-cache

A small library to add caching capabilities to localStorage.

## Install

    npm install @aadvdh/localstorage-cache

## How to use

### _import the LocalStorageCache class_

    import LocalStorageCache from 'localstorage-cache.js'

### _Initialize a new instance_

    let ls = new LocalStorageCache();

**Arguments:**
cacheExpiryTime - The amount of time to keep an item in the cache in seconds, standard at 3600 seconds, or 1 hour.
prefix - a prefix added to every key. Standard value is "lsc"

### _Add an item to the cache:_

    ls.set(key,value,cacheExpiryTime);

Will automatically clean up the cache incase the maximum storage size is exceeded.
returns true if succesful, false otherwise

**Arguments:**
key - a key to retrieve the value by. Any non alphanumeric characters are stripped.
value - the value to store. Can be any value
cacheExpiryTime - in case you want to override the cacheExpiryTime.

### _Retrieve an item from the cache:_

    ls.get(key);

returns the value if it exists and the item hasn't expired yet. Null otherwise.

**Arguments:**
key - the key of the value to be retrieved.

### _setBucket:_

    ls.setBucket(bucket);

Set the bucketname to store the items in. All non alphanumeric characters are removed from the string.

**Arguments:**
bucket - name of the bucket to use

### _Flush:_

    ls.flush(expired);

Removes keys from storage without affecting all of the localStorage. If expired is set to false it will remove every key, otherwise it will only remove expired keys.

**Arguments**
expired - Boolean value whether only expired items should be removed or all items.

### _Flush bucket_

    ls.flushBucket(expired, bucket)
    Same as flush, but will only remove from a bucket.

**Arguments:**
expired - Boolean value whether only expired items should be removed or all items.
bucket - The bucket to remove from, otherwise it will use the bucket defined within the class.
